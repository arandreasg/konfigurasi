# Menjalankan Prometheus di Docker #

```
docker run \
    -p 9090:9090 \
    -v /absolute/path/ke/prometheus.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus
```

# Menjalankan Grafana di Docker #

```
docker run -p 3000:3000 grafana/grafana
```
